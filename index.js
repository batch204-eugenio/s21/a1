let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
console.log("Original Array:")
console.log(users);



//1
function newUser(player) {
    console.log(users.push(player));
}
newUser(`John Cena`);
console.log(users);


//2
let itemFound = users[2];
function indexNum1(player1) {
    return users[player1];
    
}
indexNum1(2);
console.log(itemFound);

//3
function indexNum2() {
    let lastItemDelete = users.pop();
    console.log(lastItemDelete);
    let lastItem = users[4];
    console.log(users);
    return lastItem;
}
indexNum2();

//4
function update(index, newName) {
    users[index] = newName;
}
update(3, "Triple H");
console.log(users);


//5
function deleteAll(value, index) {
    users.splice(value, index);
}
deleteAll(0, 4);
console.log(users);



//6
function isUserEmpty() {
    if (users > 0) {
        // console.log(false);
        return false;
    } else {
        // console.log(true);
        return true;
        // console.log(return true);
    }
};
console.log(isUserEmpty()); //I always forget that I need to include the () when invoking it inside a console.log.
// isUserEmpty();
